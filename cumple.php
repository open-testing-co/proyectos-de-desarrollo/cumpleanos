<?php
require_once("bdytabla.php");
$usuario="root";
$contraseña="";
$host="localhost";
$dbname="eventos";
try{
	$pdo= new PDO("mysql:host=$host;dbname=$dbname;charset=utf8",$usuario, $contraseña);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 }catch(PDOException $ex){
  
  die($ex->getMessage());
 }
 $accion=(isset($_GET['accion']))?$_GET['accion']:'leer';
 switch($accion){
 	case 'agregar':
 	
 		$query = "INSERT INTO indra(title,descripcion,color,start) VALUES (:title,:descripcion,:color,:start)";
 		$stmt = $pdo->prepare($query);

 		$respuesta=$stmt->execute(array(
            "title"=> $_POST['title'],
            "descripcion" => $_POST['descripcion'],
            "color" => $_POST['color'],
            "start" => $_POST['start']  
        ));
        echo json_encode($respuesta);

 		break;
 	case 'eliminar':
 		$respuesta= false;
 		if(isset($_POST['id'])){
 			$query = "DELETE FROM indra WHERE id=:id";
 			$stmt = $pdo->prepare($query);
 			$respuesta=$stmt->execute(array("id"=>$_POST['id']));
        echo json_encode($respuesta);

 		}	
 		
 		break;
 	case 'modificar':
 		$query = "UPDATE indra SET
 		title=:title,
 		descripcion=:descripcion,
 		color=:color,
 		start=:start
 		WHERE id=:id";
 		$stmt = $pdo->prepare($query);
 		$respuesta=$stmt->execute(array(
 			"id" => $_POST['id'], 
            "title"=> $_POST['title'],
            "descripcion" => $_POST['descripcion'],
            "color" => $_POST['color'],
            "start" => $_POST['start']  
        ));
        echo json_encode($respuesta);
 	
 		break;
 	default:
	 	$query = "SELECT * FROM indra";
	 
		$stmt = $pdo->prepare($query);
		$stmt->execute();
		$resultado=$stmt->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode($resultado);
 		break;
 }
 //Seleccionar los eventos del calendario


?>