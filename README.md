Este proyecto nació con la necesidad de tener registrado los cumpleaños de los miembros de Canales Digitales, además de aplicar la integración continua.
Consiste en un calendario Web que te permite, agregar, borrar, modificar un evento dentro de un calendario.
Las herramientas utilizadas fueron:
-JavaScript
-jQuery
-HTML
-PHP
-MySQL